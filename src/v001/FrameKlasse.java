package v001;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class FrameKlasse extends JFrame{
	
	private double bildschirmverhaeltnis;
	private Graphics2D g2;
	
	private BufferedImage bimg;
	private int[] groessen = new int[4];
	
	private Point zoomPunkt1;
	private Point zoomPunkt2;
	
	private int zoomX1;
	private int zoomY1;
	private int zoomX2;
	private int zoomY2;
	
	
	
	FrameKlasse() {
		setTitle("Einzelbild-Vorschau");
		setUndecorated(true);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
//		v.addKeyListener(keyListn);
//		mouseListnVorschau = new VorschauMouseListenerKlasse( ( rechner.getBildschirmHoehe() * rechner.getBildschirmHoehe() ) / 60000 );
//		v.addMouseListener(mouseListnVorschau);
//		v.addMouseWheelListener(wheelListn);
		setBackground(Color.BLACK);
		bildschirmverhaeltnis = ( (double) RechnerKlasse.getBildschirmBreite() ) / ( (double) RechnerKlasse.getBildschirmHoehe() ) ;
	}
	
	public void paint(Graphics g) {
		
		if ( bimg != null ) {
		
		g2 = (Graphics2D) g;
		// 0: X - Verschiebung
		// 1: Y - Verschiebung
		// 2: Breite
		// 3: H�he
		
		if ( HauptKlasse.getZoomed() == false ) { // Standard: Einzelbild ist nicht gezoomed und soll es auch nicht werden
			
			g.clearRect(0, 0, RechnerKlasse.getBildschirmBreite(), RechnerKlasse.getBildschirmHoehe() ); // alles schw�rzen
			
			if ( ( (double) bimg.getWidth() / (double) bimg.getHeight() ) >= bildschirmverhaeltnis ) {
				groessen[2] = RechnerKlasse.getBildschirmBreite();
				groessen[0] = 0;
				groessen[3] = (int) ( ( (double) bimg.getHeight() / (double) bimg.getWidth() ) * RechnerKlasse.getBildschirmBreite() );
				groessen[1] = (int) ( ( RechnerKlasse.getBildschirmHoehe() - groessen[3] ) / 2 );
			} else {
				groessen[3] = RechnerKlasse.getBildschirmHoehe();
				groessen[1] = 0;
				groessen[2] = (int) ( ( (double) bimg.getWidth() / (double) bimg.getHeight() ) * RechnerKlasse.getBildschirmHoehe() );
				groessen[0] = (int) ( ( RechnerKlasse.getBildschirmBreite() - groessen[2] ) / 2 );
			}
			
//			System.out.println(groessen[0] +"|" +groessen[1]);
//			System.out.println(groessen[2] +" x " +groessen[3]);
			
//			groessen = RechnerKlasse.getGroessenGross(bimg.getWidth(), bimg.getHeight());
			g.drawImage(bimg, groessen[0], groessen[1], groessen[2], groessen[3], rootPane);
			
		} else { // Algorithmus zum Zoomen
			
			if ( zoomPunkt1.getX() <= zoomPunkt2.getX() ) {
				zoomX1 = (int) ( ( (double) ( ( zoomPunkt1.getX() - groessen[0] ) * (double) bimg.getWidth() ) ) / ( (double) groessen[2]) );
				zoomX2 = (int) ( ( (double) ( ( zoomPunkt2.getX() - groessen[0] ) * (double) bimg.getWidth() ) ) / ( (double) groessen[2]) );
			} else {
				zoomX1 = (int) ( ( (double) ( ( zoomPunkt2.getX() - groessen[0] ) * (double) bimg.getWidth() ) ) / ( (double) groessen[2]) );
				zoomX2 = (int) ( ( (double) ( ( zoomPunkt1.getX() - groessen[0] ) * (double) bimg.getWidth() ) ) / ( (double) groessen[2]) );
			}
			if ( zoomPunkt1.getY() <= zoomPunkt2.getY() ) {
				zoomY1 = (int) ( ( (double) ( ( zoomPunkt1.getY() - groessen[1] ) * (double) bimg.getHeight() ) ) / ( (double) groessen[3] ) );
				zoomY2 = (int) ( ( (double) ( ( zoomPunkt2.getY() - groessen[1] ) * (double) bimg.getHeight() ) ) / ( (double) groessen[3] ) );
			} else {
				zoomY1 = (int) ( ( (double) ( ( zoomPunkt2.getY() - groessen[1] ) * (double) bimg.getHeight() ) ) / ( (double) groessen[3] ) );
				zoomY2 = (int) ( ( (double) ( ( zoomPunkt1.getY() - groessen[1] ) * (double) bimg.getHeight() ) ) / ( (double) groessen[3] ) );
			} 
			// ZWISCHENSTAND: x1|y1 => oben links ; x2|y2 => unten rechts  ++  alle Koordinaten in relation zum Originalbild gebracht
			
			// Abfrage, ob angeforderte Zoompunkte �ber das Bild hinaus gehen
			if ( ( zoomX2 - zoomX1 ) > bimg.getWidth() ) {
				zoomX1 = 0;
				zoomX2 = bimg.getWidth();
			}
			if ( ( zoomY2 - zoomY1 ) > bimg.getHeight() ) {
				zoomY1 = 0;
				zoomY2 = bimg.getHeight();
			}
			
			// Skalierung, da Auswahl meistens nicht genau dem Bildschirm - Seitenverh�ltnis entspricht  
			if ( ( (double) ( zoomX2 - zoomX1 ) / (double) ( zoomY2 - zoomY1 ) ) >= bildschirmverhaeltnis ) { // relativ zu breit f�r den Bildschirm
				zoomY1 -= (int) ( ( ( (double) ( zoomX2 - zoomX1 ) / (double) bildschirmverhaeltnis ) - (double) ( zoomY2 - zoomY1 ) ) / 2.0 );
				zoomY2 += (int) ( ( ( (double) ( zoomX2 - zoomX1 ) / (double) bildschirmverhaeltnis ) - (double) ( zoomY2 - zoomY1 ) ) / 2.0 );
			} else {
				zoomX1 -= (int) ( ( ( (double) ( zoomY2 - zoomY1 ) * (double) bildschirmverhaeltnis ) - (double) ( zoomX2 - zoomX1 ) ) / 2.0 );
				zoomX2 += (int) ( ( ( (double) ( zoomY2 - zoomY1 ) * (double) bildschirmverhaeltnis ) - (double) ( zoomX2 - zoomX1 ) ) / 2.0 );
			}
			
			// Algorithmus um Rundungsfehler auszugleichen
//			System.out.println(zoomX2 +"@FrameKl. 113");
			zoomX2 = zoomX1 + ( ( ( zoomY2 - zoomY1 ) * RechnerKlasse.getBildschirmBreite() ) / RechnerKlasse.getBildschirmHoehe() );
//			System.out.println(zoomX2 +"@FrameKl. 115");
//			System.out.println("@FrameKl. 116");
			
			// Korrekturen, falls bei o.g. Skalierung negative Werte geliefert werden
			if ( zoomX2 > bimg.getWidth() ) {
				zoomX1 -= ( zoomX2 - bimg.getWidth() );
				zoomX2 = bimg.getWidth();
			}
			if ( zoomX1 < 0 ) {
				zoomX2 -= zoomX1;
				zoomX1 = 0;
			}
			if ( zoomY2 > bimg.getHeight() ) {
				zoomY1 -= ( zoomY2 - bimg.getHeight() );
				zoomY2 = bimg.getHeight();
			}
			if ( zoomY1 < 0 ) {
				zoomY2 -= zoomY1;
				zoomY1 = 0;
			}
			

			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2.drawImage(bimg, 0, 0, RechnerKlasse.getBildschirmBreite(), RechnerKlasse.getBildschirmHoehe(), zoomX1, zoomY1, zoomX2, zoomY2, rootPane);
			}
		
		}
		
		
//		16           Breite
//		-- = verh. = ------
//		 9           H�he
		
	}
	
	public void setEinzelbild(BufferedImage b) {
		bimg = b;
	}
	
	public void setZoomPunkte(Point p1, Point p2) {
		zoomPunkt1 = p1;
		zoomPunkt2 = p2;
	}
	
}
