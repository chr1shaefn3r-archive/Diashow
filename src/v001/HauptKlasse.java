package v001;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;


public class HauptKlasse {

	private static int grenzwertZoom; // Grenzwert f�r den Abstand der Zwei Punkte beim Zoomen
	private static OrdnerAuswahlKlasse ordnerAuswahl;
	private static BufferedImage[] bild;
	private static PanelKlasse[] panel;
	private static JFrame diaFenster;
	public static FrameKlasse einzelbildFenster;
	private static ProgressKlasse progressBar;
	
	private static int aktivesEinzelbild = 0;
	
	private static Listeners listeners;
	private static boolean isZoomed = false;
	
	public static void main(String[] args) throws IOException {
		
		listeners = new Listeners();
		
		ordnerAuswahl = new OrdnerAuswahlKlasse(); // JFileChooser erzeugen
		ordnerAuswahl.auswahl(); // Ordner ausw�hlen
		
		RechnerKlasse.berechneDimensionen(ordnerAuswahl.getAnzahl());
		
		initDiaFenster();
		progressBar = new ProgressKlasse(ordnerAuswahl.getAnzahl(), RechnerKlasse.getBildschirmBreite(), RechnerKlasse.getBildschirmHoehe());
		progressBar.addKeyListener(listeners.getKeyListener());
		
		bild = new BufferedImage[ordnerAuswahl.getAnzahl()];
		panel = new PanelKlasse[ordnerAuswahl.getAnzahl()];
		
		for ( int i = 0 ; i < ordnerAuswahl.getAnzahl() ; ++i ) {
			bild[i] = ImageIO.read(new File(ordnerAuswahl.getDateiNr(i)));
			panel[i] = new PanelKlasse(bild[i]);
			panel[i].addMouseListener(listeners.getMouseListener(i));
//			panel[i] = new PanelKlasse( ImageIO.read(new File(ordnerAuswahl.getDateiNr(i))) );
			
			diaFenster.getContentPane().add(panel[i]);
			progressBar.geladen();
		}
		
		progressBar.beenden();
		grenzwertZoom = RechnerKlasse.getGrenzwertZoom();
		
		initEinzelbildFenster();
		diaFenster.setVisible(true);
		
//		JFrame g = new JFrame();
//		g.setSize(800, 600);
//		g.getContentPane().setLayout(new BorderLayout());
//		g.getContentPane().add(panel[0]);
//		g.setVisible(true);
		
		
	}

	private static void initDiaFenster() {
		diaFenster = new JFrame("Diashow");
		diaFenster.setUndecorated(true);
		diaFenster.setExtendedState(JFrame.MAXIMIZED_BOTH);
		diaFenster.getContentPane().setLayout(new GridLayout(RechnerKlasse.getBildzahlY(),RechnerKlasse.getBildzahlX()));
		diaFenster.getContentPane().setBackground(Color.BLACK);
		diaFenster.addKeyListener(listeners.getKeyListener());
		diaFenster.addMouseWheelListener(listeners.getMouseWheelListener(0));
//		diaFenster.addMouseListener(listeners.getMouseListener());
	}

	private static void initEinzelbildFenster() {
		einzelbildFenster = new FrameKlasse();
		einzelbildFenster.addKeyListener(listeners.getKeyListener());
		einzelbildFenster.addMouseWheelListener(listeners.getMouseWheelListener());
		einzelbildFenster.addMouseListener(listeners.getMouseListener());
		einzelbildFenster.addMouseMotionListener(listeners.getMouseMotionListener());
		einzelbildFenster.setEinzelbild(bild[0]);
		setZoomed(false);
		einzelbildFenster.repaint();
	}

	public static void beenden() {
			System.exit(0);
	}

	public static void zeigeEinzelbild(String string, int i) {
//		System.out.println(einzelbildFenster.isVisible());
		
		if ( string.equals("relativ") ) {
//			System.out.println("Relativ aktiviert");
//			if ( ( einzelbildFenster.isVisible() == true ) || ( aktivesEinzelbild == -1 ) )  {
//				System.out.println("und wird ausgef�hrt...");

//				System.out.println("Relativ aktiviert && Fenster sichtbar oder id = -1\n(id = " +aktivesEinzelbild +")" );
				
				aktivesEinzelbild += i;
				if ( aktivesEinzelbild < 0 ) {
					aktivesEinzelbild = ordnerAuswahl.getAnzahl() - 1;
				} else if ( aktivesEinzelbild >= ordnerAuswahl.getAnzahl() ) {
					aktivesEinzelbild = 0;
				}
				
				einzelbildFenster.setEinzelbild(bild[aktivesEinzelbild]);
//			}
//			System.out.println();
		}
		
		if ( string.equals("absolut") ) {
//			System.out.println("Absolutes Bild: " +i);
			aktivesEinzelbild = i;
			einzelbildFenster.setEinzelbild( bild[aktivesEinzelbild] );
		}
		
		if ( einzelbildFenster.isVisible()) {
			setZoomed(false);
			einzelbildFenster.repaint();
		} else {
			setZoomed(false);
			einzelbildFenster.setVisible(true);
			einzelbildFenster.repaint();
//			System.out.println("Sichtbar gemacht!");
		}
		
	}
	
	public static void zeigeEinzelbildZoomed(Point p1, Point p2) {
		einzelbildFenster.setZoomPunkte(p1, p2);
		einzelbildFenster.repaint();
	}
	
	public static BufferedImage getEinzelbild() {
		return bild[aktivesEinzelbild];
	}
	
	public static void zeigeDiaFenster() {
		einzelbildFenster.setVisible(false);
	}

	public static boolean getZoomed() {
		return isZoomed;
	}

	public static void setZoomed(boolean b) {
		isZoomed = b;
	}

	public static int getGrenzwertZoom() {
		return grenzwertZoom;
	}
	
}
