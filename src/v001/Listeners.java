package v001;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;


public class Listeners {
	
	private KeyListener keyListener;
	private MouseWheelListener mouseWheelListener;
	private MouseAdapter mouseListener;
	private MouseMotionListener mouseMotionListener;
	
	
	Listeners() {
		keyListener = new KeyListener();
		mouseWheelListener = new MouseWheelListener();
		mouseListener = new MouseListener();
		mouseMotionListener = new MouseMotionListener();
	}
	
	public KeyAdapter getKeyListener() {
		return keyListener;
		
	}
	
	public MouseAdapter getMouseWheelListener(int i) {
		return new MouseWheelListener(i);
	}
	
	public MouseAdapter getMouseWheelListener() {
		return mouseWheelListener;
	}
	
	public MouseAdapter getMouseListener() {
		return mouseListener;
	}
	
	public MouseAdapter getMouseListener(int i) {
		return new MouseListener(i);
	}

	public MouseMotionListener getMouseMotionListener() {
		return mouseMotionListener;
	}
	
}

/*************************************************************************/
/* SubKlassen                                                            */
/*************************************************************************/

/*************************************************************************/
/* Maus                                                                  */
/*************************************************************************/
class MouseListener extends MouseAdapter{
	
	private int id;
	private Point p1; 
	private Point p2;
	private int abstand;
	
	MouseListener() {
		id = -666; // Willk�rliche Zahl; Zuweisung von Panels ist 0,1,2,3,...
	}
	
	MouseListener(int i) {
		id = i;
	}
	
	public void mousePressed(MouseEvent e) {
//		System.out.println( "Maus dw: " +e.getButton() );
		if ( e.getButton() == 1 ) {
			if ( id == -666 ) { // MousePress auf Einzelbild ==> Dias oder Zoom
				p1 = e.getPoint();
			} else { // MousePress auf kleines Dia ==> gro�es Einzelbild
				HauptKlasse.zeigeEinzelbild("absolut", id);
			}
		}
		if ( e.getButton() == 3 ) {
			RechnerKlasse.starteDragAufzeichnung();
		}
	}
	
	
	public void mouseReleased(MouseEvent e) {
//		System.out.println( "Maus up: " +e.getButton() );
//		System.out.println();
		if (e.getButton() == 1) {
			if ( id == -666 ) {
				p2 = e.getPoint();
				abstand = (int) p1.distance(p2);
//				System.out.println("abstand: " +abstand);
//				System.out.println("grenze: " +HauptKlasse.getGrenzwertZoom());
//				System.out.println();
				
				if (abstand >= HauptKlasse.getGrenzwertZoom() ) {
					if ( HauptKlasse.getZoomed() == false ) {
						HauptKlasse.setZoomed(true);
						HauptKlasse.zeigeEinzelbildZoomed(p1, p2);
					}
				}
				else {
					if ( HauptKlasse.getZoomed() == true ) {
						HauptKlasse.setZoomed(false);
						HauptKlasse.zeigeEinzelbild("relativ", 0);
						
					}
					else {
						HauptKlasse.zeigeDiaFenster();
					}
				}
			}
		}
		if ( e.getButton() == 3 ) {
			RechnerKlasse.beendeDragAufzeichnung();
		}
	}
}

/*************************************************************************/
/* Mausbewegung                                                          */
/*************************************************************************/
class MouseMotionListener extends MouseMotionAdapter {
	
	@Override
	public void mouseDragged(MouseEvent me) {
		if ( MouseEvent.getModifiersExText(me.getModifiersEx()).equals("Button3") ) {
			RechnerKlasse.addPunkt( me.getPoint() );
		}
	}
}

/*************************************************************************/
/* Mausrad                                                               */
/*************************************************************************/
class MouseWheelListener extends MouseAdapter {
	
	private int typ = 1;
	
	public MouseWheelListener() {
	}
	
	public MouseWheelListener(int i) {
		typ = i;
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		if ( typ == 1 ) {
			HauptKlasse.zeigeEinzelbild("relativ", e.getWheelRotation());
//			System.out.println("Rotation: " +e.getWheelRotation());
		} else {
			HauptKlasse.einzelbildFenster.setVisible(true);
		}
	}
}

/*************************************************************************/
/* Tastatur                                                              */
/*************************************************************************/
class KeyListener extends KeyAdapter {
	
	private int keyCode;
	
	public void keyReleased(KeyEvent e) {
		
//		System.out.println(e.getKeyCode());
		keyCode = e.getKeyCode();
		
		if ( keyCode == KeyEvent.VK_ESCAPE ) {
			HauptKlasse.beenden();
		}
		if ( keyCode == KeyEvent.VK_Q) {
			HauptKlasse.zeigeDiaFenster();
		}
		if (
				keyCode == KeyEvent.VK_RIGHT ||
				keyCode == KeyEvent.VK_DOWN ||
				keyCode == KeyEvent.VK_PAGE_DOWN
				){
			HauptKlasse.zeigeEinzelbild("relativ", 1);
		}
		if (
				keyCode == KeyEvent.VK_LEFT ||
				keyCode == KeyEvent.VK_UP ||
				keyCode == KeyEvent.VK_PAGE_UP
				){
			HauptKlasse.zeigeEinzelbild("relativ", -1);
		}
	}
}