package v001;

import java.io.File;

import javax.swing.JFileChooser;

@SuppressWarnings("serial")
public class OrdnerAuswahlKlasse extends JFileChooser{
	
	private int anzahl = 0; // KANN VERMUTLICH GEL�SCHT WERDEN
	private static String[] dateiListe;
	private String pfadString = "";
	
	/*
	 * Konstruktor
	 */
	OrdnerAuswahlKlasse() {
		setApproveButtonText("Ok"); // OK-Button
		setDialogTitle("Ordner ausw�hlen"); // Titel
		setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); // nur Ordner - Auswahl
		setMultiSelectionEnabled(true); // Mehrfach - Dateiauswahl
//		setCurrentDirectory(new File(System.getProperty("user.home"))); // Startverzeichnis; wobei "user.home" = "User's home directory"
		setCurrentDirectory(new File(System.getProperty("user.dir"))); // Startverzeichnis; wobei "user.dir" = "User's current working directory"
	}
	
	
	/*
	 * Auswahldialog und Auswahl auswerten
	 */
	public void auswahl() {
		
		int antwort = showDialog(null, null);
		
		if (antwort == JFileChooser.APPROVE_OPTION) {
			
			
//			System.out.println(getSelectedFiles().length +" Ordner ausgew�hlt:\n");
			
			for ( int i = 0 ; i < getSelectedFiles().length ; ++i ) { // Schleife f�r Ordner
//				System.out.println("Ordner " +(i+1) +": " +getSelectedFiles()[i]);
				
				for ( int j = 0 ; j < getSelectedFiles()[i].listFiles().length ; ++j ) { // Schleife f�r jeweils enthaltene Dateien
					
					if (getSelectedFiles()[i].listFiles()[j].isFile()) { // �berpr�fung, ob Inhalt eine Datei ist
						if ( // Abfrage auf Dateiendung
								getSelectedFiles()[i].listFiles()[j].getAbsolutePath().substring( getSelectedFiles()[i].listFiles()[j].getAbsolutePath().lastIndexOf('.')+1, getSelectedFiles()[i].listFiles()[j].getAbsolutePath().length() ).equalsIgnoreCase("jpg") ||
								getSelectedFiles()[i].listFiles()[j].getAbsolutePath().substring( getSelectedFiles()[i].listFiles()[j].getAbsolutePath().lastIndexOf('.')+1, getSelectedFiles()[i].listFiles()[j].getAbsolutePath().length() ).equalsIgnoreCase("jpeg") ||
								getSelectedFiles()[i].listFiles()[j].getAbsolutePath().substring( getSelectedFiles()[i].listFiles()[j].getAbsolutePath().lastIndexOf('.')+1, getSelectedFiles()[i].listFiles()[j].getAbsolutePath().length() ).equalsIgnoreCase("png") ||
								getSelectedFiles()[i].listFiles()[j].getAbsolutePath().substring( getSelectedFiles()[i].listFiles()[j].getAbsolutePath().lastIndexOf('.')+1, getSelectedFiles()[i].listFiles()[j].getAbsolutePath().length() ).equalsIgnoreCase("gif")
								){
							++anzahl;
							pfadString = pfadString +getSelectedFiles()[i].listFiles()[j].getAbsolutePath() +"\n"; // passende Dateien werden in einen langen String geschrieben, der dann nurnoch geteilt werden muss
						}
					}
				}
			}
		
//			System.out.println("Alle Dateien:\n" +pfadString);
//			System.out.println("Anzahl gesamt: " +anzahl +"\n");
			
			dateiListe = pfadString.split("\r\n|\r|\n"); // String mit allen Pfaden wird geteilt
			
			java.util.Arrays.sort(dateiListe); // Dateien nach Namen sortieren
			
			
		} else { // Falls abgebrochen wird
//			System.out.println("Vorgang abgebrochen!"); //NUR ZUR KONTROLLE; GESAMTE ELSE-ABFRAGE KANN GEL�SCHT WERDEN
			HauptKlasse.beenden();
		}
		
		if ( dateiListe == null || anzahl == 0 ) { // Falls sich keine Bilddateien im ausgew�hlten Ordner befinden
//			System.out.println("Keine geeigneten Dateien vorhanden!"); //NUR ZUR KONTROLLE; KANN SP�TER GEL�SCHT WERDEN
//			HauptKlasse.beenden();
			
			setCurrentDirectory(getSelectedFiles()[0]);
			auswahl();
		}
	}
	
	public int getAnzahl() {
		return anzahl;
	}
	
	public String[] getDateiListe() {
		return dateiListe;
	}
	
	public String getDateiNr(int i) {
		return dateiListe[i];
	}
}
