package v001;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelKlasse extends JPanel{
	
	private BufferedImage bimg;
	public int[] groessen = new int[4];
	
	PanelKlasse(BufferedImage img) {
		bimg = img;

		if (bimg.getWidth() >= bimg.getHeight()) {
			groessen[2] = RechnerKlasse.getDimension();
			groessen[0] = 0;
			groessen[3] = (int) ( ( (double) bimg.getHeight() / (double) bimg.getWidth() ) * RechnerKlasse.getDimension() );
			groessen[1] = (int) ( ( RechnerKlasse.getDimension() - groessen[3] ) / 2 );
		} else {
			groessen[3] = RechnerKlasse.getDimension();
			groessen[1] = 0;
			groessen[2] = (int) ( ( (double) bimg.getWidth() / (double) bimg.getHeight() ) * RechnerKlasse.getDimension() );
			groessen[0] = (int) ( ( RechnerKlasse.getDimension() - groessen[2] ) / 2 );
		}
	}
	
	public void paint(Graphics g) {
//		System.out.println("Format (beim Zeichnen): " +bimg.getWidth() +" x " +bimg.getHeight()+"\n" +
//				"X: " + groessen[0] +"\n" +
//				"Y: " + groessen[1] +"\n" +
//				"Breite: " + groessen[2] +"\n" +
//				"Hoehe: " + groessen[3] +"\n" );
		g.drawImage(bimg, groessen[0], groessen[1], groessen[2], groessen[3], getRootPane() );
		
		
//		System.out.println("Beim Zeichnen:");
//		System.out.println(groessen[0]);
//		System.out.println(groessen[1]);
//		System.out.println(groessen[2]);
//		System.out.println(groessen[3]);
//		System.out.println();
		
		
//		System.out.println("PAINT!");
//		System.out.println( RechnerKlasse.getBreiteKlein( bimg.getWidth(), bimg.getHeight()) );
//		System.out.println (RechnerKlasse.getHoeheKlein( bimg.getWidth(), bimg.getHeight()) );
		
	}
}
