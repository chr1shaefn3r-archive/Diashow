package v001;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class ProgressKlasse extends JFrame{
	
	private JProgressBar pb;
	private int max;
	private int wert = 0;
	
	ProgressKlasse(int anzahl, int breite, int hoehe) {
		
		max = anzahl;
		pb = new JProgressBar(0, max);
		setTitle("Bilder werden geladen...");
		
		setAlwaysOnTop(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(
				(int) ( breite / 2 ),
				55
				);
		setResizable(false);
		setLocation(
				(int) ( breite / 4 ) ,
				(int) ( (hoehe / 2 ) - 30 )
		);
		setLayout(new BorderLayout());
		
		
		getContentPane().add(pb, BorderLayout.CENTER);
		setVisible(true);
	}
	
	public void geladen() {
		pb.setValue(pb.getValue() + 1);
		setTitle("Bilder werden geladen...  Bild " +wert +" von " +max +" wird geladen.");
		wert += 1;
	}

	public void beenden() {
		setVisible(false);
		pb = null;
	}
	
}
