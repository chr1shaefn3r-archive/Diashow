package v001;

import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;

public class RechnerKlasse {
	
	private static int[] groessenGross = new int[4];
	
	private static int[] bildZahlY = {1,1,1};
	private static int[] bildZahlX = {1,1,1};

	private static int[] dimension = new int[3];
	
	// Fuer mouseDrag - Methoden
	private static ArrayList<Point> punktListe = new ArrayList<Point>();
	private static double[] xWerte;
	private static double[] yWerte;
	
	private static double toleranz;
	private static double mindestlaenge;
	private static double xMin;
	private static double xMax;
	private static double yMin;
	private static double yMax;
	private static double deltaX;
	private static double deltaY;
	
	public static void berechneDimensionen(int anzahl) {
		
//		System.out.println("-- SCHLEIFE 1 --");
		while (true) { // Endlos-Schleife Nr.1
			dimension[1] = (getBildschirmBreite() / bildZahlX[1]); // Dimensionsermittlung �ber BildschirmBreite
//			System.out.println("Tipp" +bildZahlX[1] +": " +dimension[1] +" ; mit " +bildZahlX[1] +" auf " +bildZahlY[1] +" Bildern.");
			if (dimension[1] > getBildschirmHoehe()) { // NO-GO: ermittelte Dimension passt nicht auf den Bildschirm
				bildZahlX[1]++; // deswegen: hochz�hlen...
				continue; // ...und wiederholen
			}
			bildZahlY[1] = getBildschirmHoehe() / dimension[1]; // Ermittlung der (vorl�ufigen) Bildzahl in Y-Richtung
			if ( ( bildZahlX[1] * bildZahlY[1] ) >= anzahl) { // Stopp, falls die (vorl�ufige) Gesamtzahl gro� genug ist
				break;
			}
			bildZahlX[1]++; // ansonsten hochz�hlen und wiederholen
		}
//		System.out.println("-- Ende Schleife 1 -- mit:");
//		System.out.println("Dimension " +dimension[1] +" und " +bildZahlX[1] +" auf " +bildZahlY[1] +" Bildern.\n");
		
		
//		System.out.println("-- SCHLEIFE 2 --");
		while (true) { // Endlos-Schleife Nr.2
			dimension[2] = (getBildschirmHoehe() / bildZahlY[2]);// Dimensionsermittlung �ber BildschirmH�he
//			System.out.println("Tipp" +bildZahlX[2] +": " +dimension[2] +" ; mit " +bildZahlX[2] +" auf " +bildZahlY[2] +" Bildern.");
			if (dimension[2] > getBildschirmBreite()) { // NO-GO: ermittelte Dimension passt nicht auf den Bildschirm
				bildZahlY[2]++; // deswegen: hochz�hlen...
				continue; // ...und wiederholen
			}
			bildZahlX[2] = getBildschirmBreite() / dimension[2];  // Ermittlung der (vorl�ufigen) Bildzahl in X-Richtung
			if ( ( bildZahlY[2] * bildZahlX[2] ) >= anzahl) { // Stopp, falls die (vorl�ufige) Gesamtzahl gro� genug ist
				break;
			}
			bildZahlY[2]++; // ansonsten hochz�hlen und wiederholen
		}
//		System.out.println("-- Ende Schleife 2 -- mit:");
//		System.out.println("Dimension " +dimension[2] +" und " +bildZahlX[2] +" auf " +bildZahlY[2] +" Bildern.\n");
		
		
		if (dimension[1] > dimension[2]) { // Gr��ere Dimension gewinnt (Ermittlung �ber H�he <--> Erm.�b. Breite)
			dimension[0] = dimension[1] - 2; // ABSTAND ZWISCHEN VORSCHAUBILDERN HIER EINSTELLEN!!!
			bildZahlX[0] = bildZahlX[1];
			bildZahlY[0] = bildZahlY[1];
//			System.out.println("--> Winnar: Schleife 1!");
			
		} else {
			dimension[0] = dimension[2] - 2; // ABSTAND ZWISCHEN VORSCHAUBILDERN HIER EINSTELLEN!!!
			bildZahlX[0] = bildZahlX[2];
			bildZahlY[0] = bildZahlY[2];
//			System.out.println("--> Winnar: Schleife 2!");
		}
		
//		System.out.println("\nIm Endeffekt: ");
//		System.out.println(dimension[0]);
//		System.out.println("mit " +bildZahlX[0] +" auf " +bildZahlY[0] +" Bildern.");
		
		
		toleranz = 0.25;
		mindestlaenge = getGrenzwertZoom();
	}
	
	public static int getBildschirmBreite() {
		return (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	}

	public static int getBildschirmHoehe() {
		return (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	}

	public static int getBildzahlX() {
		return bildZahlX[0];
	}

	public static int getBildzahlY() {
		return bildZahlY[0];
	}

	public static int getDimension() {
		return dimension[0];
	}
	
	
	public static int[] getGroessenGross(int b, int h) { // MUSS NOCH STARK VER�NDERT WERDEN!!! BILDSCHIRMVERH�LTNIS NICHT BER�CKSICHTIGT!!!!
		if (b >= h) {
			groessenGross[2] = dimension[0];
			groessenGross[0] = 0;
			groessenGross[3] = (int) ( ( (double) h / (double) b ) * dimension[0] );
			groessenGross[1] = (int) ( ( dimension[0] - groessenGross[3] ) / 2 );
		} else {
			groessenGross[3] = dimension[0];
			groessenGross[1] = 0;
			groessenGross[2] = (int) ( ( (double) b / (double) h ) * dimension[0] );
			groessenGross[0] = (int) ( ( dimension[0] - groessenGross[2] ) / 2 );
		}
		return groessenGross;
	}

	public static int getGrenzwertZoom() {
		return (int) ( ( getBildschirmBreite() * getBildschirmHoehe() ) / 14000 );
	}
	
	/*************************************************************************/
	/* Methoden fuer mouseDragged() - Events                                 */
	/*************************************************************************/
	
	public static void starteDragAufzeichnung() {
		punktListe.clear();
		punktListe.trimToSize();
	}
	
	public static void addPunkt(Point p) {
		punktListe.add(p);
	}

	public static void beendeDragAufzeichnung() {
		punktListe.trimToSize();
		
		xWerte = new double[punktListe.size()];
		yWerte = new double[punktListe.size()];
		xMin = xMax = punktListe.get(0).getX(); // als Startwert
		yMin = yMax = punktListe.get(0).getY(); // als Startwert
		
		for ( int i = 0; i < punktListe.size(); ++i ) {
			xWerte[i] = punktListe.get(i).getX();
			yWerte[i] = punktListe.get(i).getY();
			
			if ( xWerte[i] < xMin )
				xMin = xWerte[i];
			if ( xWerte[i] > xMax )
				xMax = xWerte[i];
			if ( yWerte[i] < yMin )
				yMin = yWerte[i];
			if ( yWerte[i] > yMax )
				yMax = yWerte[i];
		}
		
		erkenneBewegung(xWerte, yWerte, xMin, xMax, yMin, yMax);
		
		punktListe.clear();
		punktListe.trimToSize();
	}

	private static void erkenneBewegung(double[] x, double[] y, double x1, double x2, double y1, double y2) {
		deltaX = xMax - xMin;
		deltaY = yMax - yMin;
		
		if ( deltaX > mindestlaenge && deltaY <= ( deltaX * toleranz ) ) {
			if ( x[0] < x[x.length - 1] )
				HauptKlasse.zeigeEinzelbild("relativ", +1);
			if ( x[0] > x[x.length - 1] )
				HauptKlasse.zeigeEinzelbild("relativ", -1);
		}
		
		if ( deltaY > mindestlaenge && deltaX <= ( deltaY * toleranz ) ) {
			if ( y[0] < y[y.length - 1] )
				HauptKlasse.beenden();
			if ( y[0] > y[y.length - 1] )
				HauptKlasse.zeigeDiaFenster();
		}
	}

}